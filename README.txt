
-- SUMMARY --

The getActive module provides a block for users (anonymous or otherwise) to sign up
an organizations GetActive e-mail list.  All of the default field available in the GetActive set-up
are available upon install (no custom fields, sorry).  

For a full description of the module, visit the project page:
  http://drupal.org/project/getactive

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/getactive


-- REQUIREMENTS --

None.


-- INSTALLATION --

* untar the download file. Upload the getactive folder to your modules folder, likely sites/all/modules.  
  As Admin user (user/1) Goto the Modules page /admin/build/modules. clikc on the getactive checkbox under Other and hit submit.
  Goto the getactive domain settings page /settings/getactive/settings, 
  enter the GetActive center, 
  which can be obtained form the url when ou are logger into GetActive, i.e. domain=<your_domain>
  Enter the GetActive Host (often http:ga1.org)
  This is enough to get you up and running, goto the blocks page, admin/build/block, and 
  activate your block as you would any other lock
  


-- CONFIGURATION --

* Configure user permissions in Administer >> User management >> Permissions >>
  getactive module:

  - administer_getactive

    Users in roles with the "administer_getactive" permission will see
    be able to change the getactive settings under admin.

* Customize the domain settings in Administer >> Site configuration >>
  GetActive >> Domain Settings.
  
  	- You can add additonal blocks.
  	 
  	- You can set up source codes, users can have only one source code, 
  	and it never changes.
  	
  	- Add a default subscription, this allow syou to automatically sign a 
  	user up for a particular list in your GetActive account.
  	available lists can be found under Data Management >> Data structure
  	in you GetActive admin pages 
  	
  	- Add Remove chackbox: will add a "remove my name" checkbox to all your
  	GetActive blocks
  	
  	- Update New Information for Duplicate sign-ups, this should be self explanitory
  	
  	- Send Welcome E-mail, see above
  	
  	- Display custom Submit button.  For theming purposes you can upload
  	a customized submit button.  The custom button must be an image.
  	The module makes no attempt to restrict the size of	image you upload.
  	
* Customize what field appear in Administer >> Site configuration >>
  GetActive >> Field Settings.

	  - The e-mail field must appear and will alwys be required.
  
    - Choose The lable displyed for the field (display), 
    what fields appear in the block (Show), which are required (required),
    the order the appear in (weight, lower number float to the top), 
  
